#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# (c) 2017, Tomáš Kadlec <tomas@tomaskadlec.net>
#
# The module licensed under the terms of the The MIT License. You should
# have received a copy of the The MIT License along with the module.
# If not, see <http://opensource.org/licenses/MIT>.

from abc import ABCMeta, abstractmethod
from ansible.module_utils.basic import *
import distutils.spawn
import locale
import os
import os.path
import re
import stat
import subprocess

DOCUMENTATION = '''
---
module: parted
version_added: 1.9
short_description: Partition disks
description:
   - The module partitions disk(s) according to provided partitioning schema.
     It supports dos and GPT partition tables.
   - The module is idempotent. If a disk is partitioned according to provided
     schema no changes are made.
   - The module cannot change existing partitions. If disk partitioning differs
     from the schema an error is reported. There is an exception to this rule.
     If drop option is set to True disk partitioning is dropped. New partitions
     according to schema are created then.

options:
    device:
        description:
          - "A device to be configured. Block device is expected."
        required: true
        default: [ ]
        aliases: [ ]
    partitions:
        description:
          - "Dict containing partition specifications."
          - "Key is partition number. Each entry contains start and end at least."
        required: true
        default: [ ]
        aliases: [ ]
    drop:
        description:
          - "Drop current partitioning schema. Supported only if current label is msdos."
        required: false
        default: false
        aliases: [ ]
    label:
        description:
          - "Disk label - 'msdos' or 'gpt' are supported"
        required: false
        default: msdos
        aliases: [ ]
author: Tomas Kadlec
'''

EXAMPLES = '''
'''

locale.setlocale(locale.LC_ALL, 'C')


class ValidationException(Exception):
    """
    Custom validation exception. Violations are stored as a list under violations property.
    """

    def __init__(self, message, violations):
        super(Exception, self).__init__(message)
        self.violations = violations


class InvalidSchemaException(ValidationException):
    pass


class InvalidDiskException(ValidationException):
    pass


class InvalidPartitionException(ValidationException):
    pass


class Container:
    """
    Common predecessor for Disk and Schema. Has a label, partitions and provides default values.
    """
    __metaclass__ = ABCMeta

    def __init__(self):
        self.default_label = 'msdos'
        self.supported_labels = ['msdos', 'gpt']
        self.required = ['start', 'end']
        self.compare = ['start', 'end']
        self.defaults = {'id': '83', 'type': 'primary'}
        self.label = None
        self.partitions = dict()


class Partition:
    """
    Represent a partition (in a schema or on a disk)
    """

    def __init__(self, required=[], compare=[], defaults={}, **kwargs):
        self.required = required
        self.compare_keys = compare
        self.defaults = defaults
        self._defaults(kwargs)
        self._validate(kwargs)
        for key, value in kwargs.iteritems():
            setattr(self, key, value)

    def _defaults(self, kwargs):
        for key, value in self.defaults.iteritems():
            kwargs[key] = value

    def _validate(self, kwargs):
        violations = list()
        for key in self.required:
            if key not in kwargs:
                violations.append("Partition missing required key *%s*." % key)
        if violations:
            raise InvalidPartitionException("Invalid partition. See violations for details.", violations)

    def __str__(self):
        result = ""
        for key in self.required:
            result = "%s %s=%s" % (result, key, getattr(self, key))
        return result

    def compare(self, number, partition):
        """
        Compare two partitions using compare list of the first one
        :param number: Partition number
        :param partition:
        :type partition: Partition
        :return: True|list<string>
        """
        violations = list()
        for key in self.compare_keys:
            if not hasattr(partition, key):
                violations.append("%s: Schema does not contain key %s." % (number, key))
            elif getattr(self, key) != getattr(partition, key) and getattr(partition, key) != "-1":
                violations.append("%s: Value differs. Current: %s Expected: %s" %
                                  (number, getattr(self, key), getattr(partition, key)))
        return violations


class Schema(Container):
    """
    Represents desired disk partitioning schema
    """

    def __init__(self, label, partitions):
        Container.__init__(self)
        violations = list()
        try:
            if not label:
                label = self.default_label
            self._validate(label)
        except InvalidSchemaException as e:
            violations.append(e)
        self.required.extend(['id', 'type'])
        if isinstance(partitions, str):
            import json
            partitions = json.loads(partitions)
        for number, spec in partitions.iteritems():
            try:
                partitions[number] = Partition(self.required, self.compare, self.defaults, **spec)
            except InvalidPartitionException as e:
                violations.append(e)
        if violations:
            raise InvalidSchemaException("Schema contains errors. Check violations property.", violations)
        self.label = label
        self.partitions = partitions

    def _validate(self, label):
        if label not in self.supported_labels:
            raise InvalidSchemaException("Unsupported label %s" % label)


class Disk(Container):
    """
    Represents a disk
    """
    state_unknown = 0
    state_invalid = 1
    state_empty = 2
    state_valid = 3

    def __init__(self, device, schema, executor):
        """
        :param device: Block device
        :param schema: Desired disk schema
        :type schema: Schema
        :param executor: A tool to use to examine disk and make changes
        :type executor: Executor
        """
        Container.__init__(self)
        self._validate(device)
        self.device = device
        self.schema = schema
        self.state = self.state_unknown
        self.violations = list()
        self.executor = executor
        self.executor.examine(self)
        self._check(self.schema, self.violations)

    def _check(self, schema, violations=[]):
        """
        Checks if current partitions are as expected
        :return:
        """
        if not (self.label and self.partitions):
            self.state = self.state_empty
        else:
            if self.label != schema.label:
                violations.append("Label differs. Current: %s Expected: %s" % (self.label, schema.label))

            for number, partition in self.partitions.iteritems():
                if number not in schema.partitions:
                    violations.append("Partition %s exists but it is not defined in schema." % number)
                else:
                    violations.extend(partition.compare(number, schema.partitions[number]))

            for number in schema.partitions.keys():
                if number not in self.partitions:
                    violations.append("Partition %s does not exit but it is defined in schema." % number)

            if violations:
                self.state = self.state_invalid
            else:
                self.state = self.state_valid

        return self.state

    def create(self):
        """
        Creates partitions according to schema
        :return:
        """
        self.executor.create(self)

    def drop(self):
        """
        Wipes out partition table
        :return:
        """
        self.executor.drop(self)

    @staticmethod
    def _validate(device):
        if not (os.path.exists(device) and stat.S_ISBLK(os.stat(device).st_mode)):
            raise InvalidDiskException("Device %s does not exist or is not a block device." % device)


class Executor:
    """
    The class provides an interface that all executors must follow.
    """

    def __init__(self):
        pass

    __metaclass__ = ABCMeta

    @abstractmethod
    def examine(self, disk):
        """
        Examine current partitioning scheme of a disk
        :param disk: A disk to use
        :type disk: Disk
        :return:
        """
        pass

    @abstractmethod
    def create(self, disk, schema):
        """
        Create new partition scheme
        :param disk: A disk to use
        :type disk: Disk
        :type schema: A schema to apply
        :param schema: Schema
        :return:
        """
        pass

    @abstractmethod
    def drop(self, disk):
        """
        Drop disk schema
        :param disk: A disk to use
        :type disk: Disk
        :return:
        """
        pass


class PartedExecutor(Executor):
    supported_labels = ['msdos', 'gpt']

    def __init__(self):
        super(Executor, self).__init__()
        self.cmd = distutils.spawn.find_executable('parted')
        if not self.cmd:
            raise RuntimeError("Unable to find GNU/Parted executable (in PATH).")
        self.sfdisk = distutils.spawn.find_executable('sfdisk')
        self.sgdisk = distutils.spawn.find_executable('sgdisk')

    def examine(self, disk):
        """
        Examine disk
        :param disk: Disk to examine
        :type disk: Disk
        :return:
        """
        text_schema = self._examine_disk(disk.device)
        # 1st line - skip, 2nd line - label, 3rd line and next - partitions
        text_schema = text_schema.splitlines()
        self._examine_label(disk, text_schema)
        self._examine_partitions(disk, text_schema)
        self._examine_partitions_sfdisk(disk)

    # run parted to get disk information
    def _examine_disk(self, device):
        try:
            return subprocess.check_output([self.cmd, '-s', '-m', device, 'unit', 'MiB', 'print'])
        except subprocess.CalledProcessError as e:
            if not (e.returncode == 1 and e.output):
                raise RuntimeError(e.message)
            return e.output

    # extract label from 2nd line
    # examples:
    # /dev/sda:476940MiB:scsi:512:4096:unknown:ATA ST500LM000-1EJ16:;
    # /dev/cciss/c0d0:69974MiB:cpqarray:512:512:msdos:Compaq Smart Array:;
    @staticmethod
    def _examine_label(disk, schema):
        try:
            line = schema[1]
            line = line.split(':')
            if line[5] == 'unknown':
                disk.label = None
            elif line[5] in disk.supported_labels:
                disk.label = line[5]
            else:
                raise InvalidDiskException("Unsupported disk label.")
        except IndexError:
            raise InvalidDiskException("Failed to set disk label. Unexpected output.")

    # extract partitions from 3rd line and next
    # example:
    # 1:1.00MiB:122MiB:121MiB:ext4::boot;
    # 2:123MiB:69973MiB:69850MiB:::;
    # 5:123MiB:15747MiB:15624MiB:linux-swap(v1)::;
    # 6:15748MiB:69973MiB:54225MiB:ext4::;
    @staticmethod
    def _examine_partitions(disk, schema):
        lines = schema[2:]
        for line in lines:
            line = line.split(':')
            try:
                partition = Partition(disk.required, disk.compare, {}, **{'start': line[1], 'end': line[2]})
                disk.partitions[line[0]] = partition
            except IndexError:
                pass

    # update schema using information from other commands (sfdisk)
    # example output:
    # /dev/cciss/c0d0p1:start=2048,size=247808,type=83,bootable
    # /dev/cciss/c0d0p2:start=251902,size=143052802,type=5
    # /dev/cciss/c0d0p5:start=251904,size=31997952,type=82
    # /dev/cciss/c0d0p6:start=32251904,size=111052800,type=8
    def _examine_partitions_sfdisk(self, disk):
        if disk.label == 'msdos':
            try:
                output = subprocess.check_output([self.sfdisk, '-d', disk.device])
                for entry in re.findall('^/dev/.*(\d+)\s+:.*type=(\d+)', output, re.M):
                    try:
                        setattr(disk.partitions[entry[0]], 'id', entry[1])
                    except:
                        pass
            except:
                pass

    def create(self, disk):
        violations = list()
        try:
            subprocess.check_output(['parted', '-s', disk.device, 'mklabel', disk.schema.label])
        except subprocess.CalledProcessError as e:
            violations.append("Failed to make label %s. Code: %s: %s" % (disk.schema.label, e.returncode, e))

        for number, partition in sorted(disk.schema.partitions.items()):
            try:
                swap = ''
                if hasattr(partition, 'swap'):
                    swap = 'linux-swap'
                subprocess.check_output(
                    ['parted', '-s', '-a', 'optimal', '--', disk.device, 'mkpart', partition.type, swap,
                     partition.start, partition.end])
                if disk.schema.label == 'gpt' and hasattr(partition, 'bios_boot'):
                    subprocess.check_output(['parted', '-s', disk.device, 'set', number, 'bios_grub', 'on'])
                if hasattr(partition, 'boot'):
                    subprocess.check_output(['parted', '-s', disk.device, 'set', partition, 'boot', 'on'])
            except subprocess.CalledProcessError as e:
                violations.append("Failed to make partition %s. Code: %s: %s" % (number, e.returncode, e))

        try:
            subprocess.check_output(['partprobe', disk.device])
        except subprocess.CalledProcessError as e:
            violations.append("Partprobe failed. Restart host. Code: %s: %s" % (e.returncode, e))

        if violations:
            raise InvalidDiskException("Failed to create label/or partitions.", violations)

    def drop(self, disk):
        try:
            if not disk.label or disk.label == 'msdos':
                subprocess.check_output(['dd', 'if=/dev/zero', 'of=' + disk.device, 'bs=1M', 'count=1'])
            else:
                raise RuntimeError("Drop is not supported for current disk label: %s" % disk.label)
        except subprocess.CalledProcessError as e:
            raise InvalidDiskException("Failed to drop old disk partitioning schema. Code: %s: %s" % (e.returncode, e))


module = AnsibleModule(
    argument_spec=dict(
        device=dict(required=True),
        partitions=dict(required=True),
        drop=dict(required=False, default=False),
        label=dict(required=False, default='msdos')
    )
)


def main():
    try:
        drop = False
        if "drop" in module.params and module.params["drop"] == "True":
            drop = True
        executor = PartedExecutor()
        schema = Schema(module.params["label"], module.params["partitions"])
        disk = Disk(module.params["device"], schema, executor)

        if disk.state == Disk.state_invalid and drop is False:
            module.fail_json(msg="Disk has invalid partitioning scheme.", violations=disk.violations)

        changed = False
        changes = {'state': disk.state}

        if disk.state == Disk.state_valid:
            pass

        elif disk.state == Disk.state_empty:
            disk.create()
            changed = True

        elif disk.state == Disk.state_invalid and drop is True:
            disk.drop()
            disk.create()
            changed = True
            changes['violations'] = disk.violations

        module.exit_json(changed=changed, changes=changes)
    except Exception as e:
        module.fail_json(msg="Module failed because of exception: %s (%s, line %s)" % (e, type(e), sys.exc_info()[-1].tb_lineno))

main()
