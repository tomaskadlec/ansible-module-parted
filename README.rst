ansible-module-parted
=====================

.. _Ansible: https://docs.ansible.org
.. _`GNU/Parted`: http://www.gnu.org/software/parted/

This project aims to create an Ansible module for partitioning disks. The module
uses `GNU/Parted`_ to examine disks and to partition them. It supports *dos* and
*GPT* partition tables.

Requirements
------------

* Ansible_ 2 or higher
* `GNU/Parted`_ must be installed on managed hosts

Installation
------------

There are two possibilities how to install the module. Choose one that suits you
best.

#. Copy the role into the ``roles`` subdirectory: ::

     cd YOUR_PROJECT
     mkdir -p library/parted
     wget -O - https://gitlab.com/tomaskadlec/ansible-module-parted/repository/archive.tar.bz2 \
       | tar xjf - --wildcards -C library/parted --strip=1

#. If your project uses git you can make use of git submodules: ::

     cd YOUR_PROJECT
     mkdir -p library
     git submodule add git@gitlab.com:tomaskadlec/ansible-module-parted.git library/parted

Usage
-----

The module expects information about a device, partition table and partitions.

``device``
    A device that should be partitioned. Use ``with_items`` to partition
    multiple devices.

``label``
    Partition label, allowed values are ``dos`` and ``gpt``. Defaults to
    ``dos``.

``partitions``
    A dict that contains specs for each partition. The key must be an integer -
    number of the respective partition. Each entry consists of:

    * ``name`` - name of the partition,
    * ``start``, ``end`` - start end end of the paritition, (e.g. 1MiB, 129MiB,
      use -1 for the rest of the space left),
    * ``type`` (only for *dos*) - ``primary`` or ``extended``,
    * ``id`` - type of the partition in hexadecimal notation (defaults to 83),
    * ``boot`` - set boot flag
    * ``bios_boot`` - set boot flag (``gpt`` label only with protective MBR).

``drop``
    If the option is set to ``True`` existing partitions will be dropped and new
    partitions will be created according to ``partitions``. Defaults to ``False``.

The module works as follows. It checks if disk is already partitioned at first
and computes its state.  Possible states are *invalid*, *empty* and *valid*. An
operation is chosen based on the state and value of ``drop`` option then. 

*valid*
  No change is reported task succeeds.

*empty*
  Disk is partitioned according to provided schema. If no error occurs during
  changes task succeeds.

*invalid*, ``drop`` is ``False``
  An error is reported back and task fails.

*invalid* ``drop`` is ``True``
  Current disk schema is deleted and new one is created. If no error occurs
  during changes task succeeds.

Please note that ``partitions`` must be serialized as JSON. There is an issue
with passing structured data with Ansible_. Always use ``MiB`` as unit. No
conversions are performed!

Example usage: ::
    
    vars:
        disks:
            -
                device: /dev/sda
                label: msdos
                partitions:
                    '1': {name: docker, fs: btrfs, start: '1.00MiB', end: '65536MiB'}
                    '2': {name: gluster, fs: xfs, start: '65536MiB', end: '-1'}
    tasks:
        -
            parted:
                device: "{{item.device}}"
                label: "{{item.label}}"
                partitions: "{{item.partitions|to_json}}"
            with_items: "{{disks}}"


.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
